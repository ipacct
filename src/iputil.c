/* This file is part of IPACCT
   Copyright (C) 1999,2000,2001,2002,2003,2004,2005,2008 Sergey Poznyakoff

   Ipacct is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   Ipacct is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Ipacct.  If not, see <http://www.gnu.org/licenses/>. */

#if defined(HAVE_CONFIG_H)
# include <config.h>
#endif

#include <stdlib.h>
#include <netinet/in.h>
#include <netdb.h>
#include "ipacct.h"

/* ***********************************************************************
 * IP address fiddling 
 *************************************************************************/

/*
 *	Return a printable host name (or IP address in dot notation)
 *	for the supplied IP address.
 */
char *
ip_hostname(IPADDR ipaddr)
{
	struct		hostent *hp;
	static char	hstname[128];

	hp = gethostbyaddr((char *)&ipaddr, sizeof(struct in_addr), AF_INET);
	if (hp == 0) {
		ipaddr2str(hstname, ipaddr);
		return hstname;
	}
	return (char *)hp->h_name;
}


/*
 *	Return an IP address in host long notation from a host
 *	name or address in dot notation.
 */
IPADDR
get_ipaddr(char *host)
{
	struct hostent	*hp;
	IPADDR ipstr2long();

	if (good_ipaddr(host) == 0) 
		return ipstr2long(host);
	else if ((hp = gethostbyname(host)) == (struct hostent *)NULL) 
		return((IPADDR)0);
	/* else */
	return *(IPADDR *)hp->h_addr;
}


/*
 *	Check for valid IP address in standard dot notation.
 */
int
good_ipaddr(char *addr)
{
	int	dot_count;
	int	digit_count;

	dot_count = 0;
	digit_count = 0;
	while (*addr != '\0' && *addr != ' ') {
		if (*addr == '.') {
			dot_count++;
			digit_count = 0;
		} else if (!isdigit(*addr)) {
			dot_count = 5;
		} else {
			digit_count++;
			if(digit_count > 3) {
				dot_count = 5;
			}
		}
		addr++;
	}
	if (dot_count != 3) 
		return -1;
	else 
		return 0;
}


/*
 *	Return an IP address in standard dot notation for the
 *	provided address in host long notation.
 */
char *
ipaddr2str(char *buffer, IPADDR ipaddr)
{
	int	addr_byte[4];
	int	i;
	IPADDR	xbyte;

	ipaddr = ntohl(ipaddr);
	for (i = 0; i < 4; i++) {
		xbyte = ipaddr >> (i*8);
		xbyte = xbyte & (IPADDR)0x000000FF;
		addr_byte[i] = xbyte;
	}
	sprintf(buffer, "%u.%u.%u.%u", addr_byte[3], addr_byte[2],
		addr_byte[1], addr_byte[0]);
	return buffer;
}


/*
 *	Return an IP address in host long notation from
 *	one supplied in standard dot notation.
 */
IPADDR
ipstr2long(char *ip_str)
{
	char	buf[6];
	char	*ptr;
	int	i;
	int	count;
	IPADDR	ipaddr;
	int	cur_byte;

	ipaddr = (IPADDR)0;
	for (i = 0; i < 4; i++) {
		ptr = buf;
		count = 0;
		*ptr = '\0';
		while (*ip_str != '.' && *ip_str != '\0' && count < 4) {
			if (!isdigit(*ip_str)) {
				return (IPADDR)0;
			}
			*ptr++ = *ip_str++;
			count++;
		}
		if (count >= 4 || count == 0) {
			return (IPADDR)0;
		}
		*ptr = '\0';
		cur_byte = atoi(buf);
		if (cur_byte < 0 || cur_byte > 255) {
			return (IPADDR)0;
		}
		ip_str++;
		ipaddr = ipaddr << 8 | (IPADDR)cur_byte;
	}
	return htonl(ipaddr);
}

