/* This file is part of IPACCT
   Copyright (C) 1999,2000,2001,2002,2003,2004,2005,2008 Sergey Poznyakoff

   Ipacct is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   Ipacct is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Ipacct.  If not, see <http://www.gnu.org/licenses/>. */

#if defined(HAVE_CONFIG_H)
# include <config.h>
#endif

#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/time.h>
#include <pcap.h>
#include <netinet/in.h>
#include <string.h>
#include <errno.h>
#include "ipacct.h"

unsigned long total_bytes = 0;

struct callback {
	pcap_handler    function;
	int             type;
};


static struct callback callbacks[] = {
	{cons_ether_packet, DLT_EN10MB},
	{cons_ether_packet, DLT_IEEE802 },
	{cons_raw_packet, DLT_RAW},
	{cons_ppp_packet, DLT_PPP},
	{NULL, DLT_FDDI},
	{NULL, DLT_SLIP},
	{NULL, DLT_NULL},
	{NULL, 0},
};


void
run(pcap_t *p, pcap_handler callback)
{
	int pfd = 0;
	fd_set readmask;
	time_t start, now;
	struct timeval wait;
	int dumped = 0;

	pfd = pcap_fileno(p);
	
	start = time(NULL);

	while (1) {
		FD_ZERO(&readmask);
		FD_SET(pfd, &readmask);
		wait.tv_sec = 1; wait.tv_usec = 0;
		if (select(pfd+1, &readmask, NULL, NULL, &wait) < 0) {
			if (errno == EINTR)
				continue;
			die(1, "failed in select: %s", strerror(errno));
		}
		now = time(NULL);
			
		if (FD_ISSET(pfd, &readmask))
			pcap_read(p, -1, callback, NULL);
		
		wait.tv_sec = 1; wait.tv_usec = 0;
		if (fixed_clocks) {
			if (!dumped) {
				if (now % slice_interval == 0) {
					dump_table(now);
					dumped = 1;
				}
			} else if (now % slice_interval)
				dumped = 0;
		} else if (now - start > slice_interval) {
			dump_table(now);
			start = now;
		}
	}
}

pcap_handler
lookup_pcap_callback (int type)
{
	pcap_handler retn = NULL;
	struct callback *callback;

	for (callback = callbacks; callback->function; callback++)
		if (type == callback->type) {
			retn = callback->function;
			break;
		}
	
	return retn;
}
