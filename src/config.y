%{
/* This file is part of IPACCT
   Copyright (C) 1999,2000,2001,2002,2003,2004,2005,2008 Sergey Poznyakoff 

   Ipacct is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   Ipacct is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Ipacct.  If not, see <http://www.gnu.org/licenses/>. */

#if defined(HAVE_CONFIG_H)
# include <config.h>
#endif

#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <sys/stat.h>
#include <stdarg.h>
#include <netdb.h>
#include <syslog.h>
#include <string.h>
#include <ipacct.h>
	
#define YYDEBUG 1 
#define PARSER_BUFSIZE 128
	/* Note: currently these should be the same: */
#define MAX_STR_LEN PARSER_BUFSIZE
	
#define OPT_EXACT 1
	
struct keyword {
	char *name;
	int val;
};

struct keyword options[] = {
	"exact", OPT_EXACT,
	NULL,    0
};
 
int get_mask_by_length(int len, IPADDR *);
int read_address(char **, IPADDR *);
 
static int xlat_keyword(struct keyword *kw, char *str);
static void yyerrsync();
static void register_address(char *name, IPADDR addr, UINT4 netmask);
void parse_err(char *fmt, ...);
%}

%union {
	long number;
	char string[MAX_STR_LEN];
	UINT4 ipaddr;
	network_t network;
	list_t list;
	stream_t stream;
	flow_t flow;
}

%token <number> NUMBER
%token <string> STRING
%token <ipaddr> IPADDRESS
%token STAT HOST NET NETMASK
%token STREAM IS FROM TO ALL EXCEPT ITSELF IN OUT AS
%token DEFINE
%token SLICE OPTION
%token CHANNEL K_FILE K_SYSLOG UDP
%token <number> FACILITY PRIORITY
%token DUMPFILE
%token ABSOLUTE
%token BOGUS
%token EOL
%token USER

%type <number> timeval
%type <ipaddr> host
%type <number> port
%type <number> facility priority
%type <network> network
%type <number> channel_opts

%type <string> strnam
%type <network> address
%type <list> addr_list to_addr_list from_addr_list all_addr_list
%type <number> flow_dir
%type <flow> flow_decl
%type <stream> stream_decl stream_def
%type <list> stream_decl_list
%%
            
input       : list
            ;

list        : stmt
            | list stmt
            ;

stmt        : stat_stmt 
            | dump_stmt 
            | host_stmt 
            | stream_stmt
            | defn_stmt 
            | user_stmt
            | error /* Actually it is `error EOL' (see yyerrsync) */
              {
		      yyerrsync();
		      yyerrok;
		      yyclearin;
	      }
            ;

user_stmt   : USER STRING
              {
		      user = strdup ($2);
	      }
            ;

stat_stmt   : STAT stat_cmd
            ;

stat_cmd    : SLICE timeval
              {
		      slice_interval = $2;
	      }
            | OPTION STRING
              {
		      switch (xlat_keyword(options, $2)) {
		      case OPT_EXACT:
			      fixed_clocks++;
			      break;
		      default:
			      parse_err("%s:%d: unknown option");
		      }
	      }		   
            | CHANNEL channel
            ;

timeval     : NUMBER
            | NUMBER ':' NUMBER
              {
		      $$ = $1*60+$3;
	      }
            | NUMBER ':' NUMBER ':' NUMBER
              {
		      $$ = ($1*60+$3)*60+$5;
	      }
            ;

channel     : K_FILE STRING channel_opts
              {
		      if (add_file_channel($2, $3))
			      error("too many channels");
	      }
            | K_SYSLOG facility '.' priority channel_opts
              {
		      if (add_syslog_channel($5, $2, $4))
			      error("too many channels");
	      }
            | UDP host port channel_opts
              {
		      if (add_udp_channel($2, $3, $4))
			      error("too many channels");
	      }
            ;

facility    : NUMBER
            | FACILITY
            ;

priority    : NUMBER
            | PRIORITY
            ;

channel_opts: /* empty */
              {
		      $$ = 0;
	      }
            | ABSOLUTE
              {
		      $$ = 1;
	      }
            ;

host        : IPADDRESS
            | STRING
              {
		      struct hostent *hp;
		      if ((hp = gethostbyname($1)) == NULL) { 
			      parse_err("host not found: %s", $1);
			      YYERROR;
		      }
		      $$ = *(IPADDR*)hp->h_addr;
	      }
            ;

port        : NUMBER
              {
		      $$ = ntohs($1);
	      }
            | STRING
              {
		      struct servent *s;

		      s = getservbyname($1, "udp");
		      if (s)
			      $$ = s->s_port;
		      else {
			      parse_err("no such service: %s", $1);
			      YYERROR;
		      }
	      }
            ;

dump_stmt   : DUMPFILE STRING
              {
		      strcpy(dump_name, $2);
	      }
            ;

/* Backward compatibility */
host_stmt   : HOST IPADDRESS
              {
		      char addrbuf[ASCII_IP_LENGTH];
		      ipaddr2str(addrbuf, $2);
		      register_address(addrbuf, $2, ALLONES);
	      }
            | NET network
              {
		      char addrbuf[ASCII_IP_LENGTH];
		      ipaddr2str(addrbuf, $2.addr);
		      register_address(addrbuf, $2.addr, $2.netmask);
	      }
            ;

network     : IPADDRESS '/' NUMBER
              {
		      $$.addr = $1;
		      get_mask_by_length($3, &$$.netmask);
	      }
            | IPADDRESS NETMASK IPADDRESS
              {
		      $$.addr = $1;
		      $$.netmask = $3;
	      }
            ;

stream_stmt : STREAM strnam is address 
              {
		      /* FIXME: take care of ALL and ITSELF */
		      register_address($2, $4.addr, $4.netmask);
	      }
            | STREAM strnam is stream_decl_list
              {
		      register_stream_list($2, $4);
	      }
            ;

strnam      : STRING
            ;

is          : /* empty */
            | IS
            ;

stream_decl_list: stream_decl
              {
		      $$ = NULL;
		      list_alloc(&$$, &$1, sizeof($1));
	      }
            | stream_decl_list stream_decl
              {
		      list_alloc(&$1, &$2, sizeof($2));
		      $$ = $1;
	      }
            ;

as          : /* empty */
            | AS
            ;

stream_decl : stream_def as flow_dir
              {
		      $$ = $1;
		      $$.dir = $3;
	      }
            ;

stream_def  : flow_decl
              {
		      $$.direct = $1;
		      $$.except.src = NULL;
		      $$.except.dst = NULL;
	      }
            | flow_decl EXCEPT flow_decl
              {
		      $$.direct = $1;
		      $$.except = $3;
	      } 
           ;

all_addr_list: /* empty */
             {
		     network_t all = {0, 0};
		     $$ = NULL;
		     list_alloc(&$$, &all, sizeof(all));
	     }
           ;

to_addr_list: all_addr_list
           | TO addr_list
             {
		     $$ = $2;
	     }
           ;

from_addr_list: all_addr_list
           | FROM addr_list
             {
		     $$ = $2;
	     }
           ;

flow_decl  : FROM addr_list to_addr_list
             {
		     $$.src = $2;
		     $$.dst = $3;
	     }
           | TO addr_list from_addr_list
             {
		     $$.src = $3;
		     $$.dst = $2;
	     }
           ;

flow_dir   : IN
             {
		     $$ = DIR_SRC;
	     }
           | OUT
             {
		     $$ = DIR_DST;
	     }
           ;

addr_list  : address
             {
		     $$ = NULL;
		     list_alloc(&$$, &$1, sizeof($1));
	     }
           | addr_list address
             {
		     list_alloc(&$1, &$2, sizeof($2));
		     $$ = $1;
	     }
           ;

address    : IPADDRESS
             {
		     $$.addr = $1;
		     $$.netmask = ALLONES;
	     }
           | network
           | ALL
             {
		     $$.addr = 0;
		     $$.netmask = 0;
	     }
           ;

defn_stmt  : DEFINE strnam addr_list
             {
		     /*   install_netlist($2, $3);*/
	     }
           ;

%%

FILE *file;
char buffer[PARSER_BUFSIZE];
char *curp = NULL;
static int line;
static char *filename;

void
parse_err(char *fmt, ...)
{
	va_list ap;
	char buf[512];
	
	va_start(ap, fmt);
	sprintf(buf, "%s:%d: ", filename, line);
	vsprintf(buf+strlen(buf), fmt, ap);
	error("%s", buf);
	va_end(ap);
}
		
int
yyerror(char *s)
{
	parse_err(s);
	return 0;
}

int
xlat_keyword(struct keyword *kw, char *str)
{
	for (; kw->name; kw++)
		if (strcmp(kw->name, str) == 0)
			break;
	
	return kw->val;
}

int
read_address(char **line_ptr, IPADDR *addr_ptr)
{
	char buffer[17];
	char *startp = *line_ptr;
	char *ptr, *endp;
	IPADDR addr;
		
	for (ptr = buffer, endp = startp; *endp; endp++, ptr++)
		if (!(isdigit(*endp) || *endp == '.')) 
			break;
		else if (endp < startp + sizeof(buffer) - 1)
			*ptr = *endp;
		else {
			*line_ptr = ptr;
			return -1;
		}

	*ptr = 0;
	*line_ptr = endp;
	
	if ((addr = get_ipaddr(buffer)) == 0) 
		return -1;
	*addr_ptr = addr;
	return 0;
}

int
get_mask_by_length(int len, IPADDR *return_mask)
{
	UINT4 mask;
	
	if (len <= 0 || len > 32) {
		return -1;
	}
		
	mask = 0xfffffffful >> (32-len);
	mask <<= (32-len);
	mask = ntohl(mask);/*FIXME?*/
	*return_mask = mask;
	return 0;
}

struct keyword keywords[] = {
	"host",     HOST,
	"net",      NET,
	"stat",     STAT,
	"slice",    SLICE,       
	"option",   OPTION,      
	/*"debug",    DEBUG,       */
	"dumpfile", DUMPFILE,
	"netmask",  NETMASK,
	"mask",     NETMASK,
	"channel",  CHANNEL,
	"file",     K_FILE,
	"syslog",   K_SYSLOG,
	"udp",      UDP,
	"absolute", ABSOLUTE,
	"stream",   STREAM,
	"is",       IS,
	"from",     FROM,
	"to",       TO,
	"all",      ALL,
	"any",      ALL,
	"except",   EXCEPT,
	"but",      EXCEPT,
	"itself",   ITSELF,
	"define",   DEFINE,
	"as",       AS,
	"input",    IN,
	"output",   OUT,
	"user",     USER,
	NULL, 0
};

struct keyword facility_kw[] = {
	"user",      LOG_USER,
	"daemon",    LOG_DAEMON,
	"auth",      LOG_AUTH,
	"local0",    LOG_LOCAL0,
	"local1",    LOG_LOCAL1,
	"local2",    LOG_LOCAL2,
	"local3",    LOG_LOCAL3,
	"local4",    LOG_LOCAL4,
	"local5",    LOG_LOCAL5,
	"local6",    LOG_LOCAL6,
	"local7",    LOG_LOCAL7,
	NULL, -1
};

struct keyword priority_kw[] = {
	"emerg",     LOG_EMERG,
	"alert",     LOG_ALERT,
	"crit",      LOG_CRIT,
	"err",       LOG_ERR,
	"warning",   LOG_WARNING,
	"notice",    LOG_NOTICE,
	"info",      LOG_INFO,
	"debug",     LOG_DEBUG,
	NULL, -1
};
	
int
yylex()
{
	int n;
	char *q;
	UINT4 ipaddr;
  again:
	if (!curp || !*curp) 
		curp = fgets(buffer, sizeof(buffer), file);

	if (!curp)
		return 0;

	while (*curp && (*curp == ' ' || *curp == '\t'))
		curp++;

	if (*curp == '#') {
		line++;
		*curp = 0;
		goto again;
	}
	
	if (*curp == '"') {
		++curp;
		q = yylval.string;
		while (*curp != '"') {
			if (*curp == 0 || *curp == '\n') {
				parse_err("missing closing quote");
				*q = 0;
				return BOGUS;
			}
			*q++ = *curp++;
		}
		*q = 0;
		++curp;
		return STRING;
	}

	if (isdigit(*curp)) {
		char *p;
		
		n = strtol(curp, &p, 0);
		if (*p == '.') {
			if (read_address(&curp, &ipaddr)) {
				parse_err("invalid IP address near `%s'",
					  curp);
				return BOGUS;
			}
			yylval.ipaddr = ipaddr;
			return IPADDRESS;
		}
		curp = p;
		yylval.number = n;
		return NUMBER;
	}

	if (isalpha(*curp)) {
		q = yylval.string;
		while (!(isspace(*curp) || ispunct(*curp)))
			*q++ = *curp++;
		*q = 0;

		n = xlat_keyword(keywords, yylval.string);
		if (n == 0) {
			n = xlat_keyword(facility_kw, yylval.string);
			if (n != -1) {
				yylval.number = n;
				return FACILITY;
			}
			n = xlat_keyword(priority_kw, yylval.string);
			if (n != -1) {
				yylval.number = n;
				return PRIORITY;
			}
			return STRING;
		}
		return n;
	}
	
	if (*curp == '\n') { 
		line++;
		curp++;
		goto again;
	}
		
	return *curp++;
}

/* A rudimentary attempt to syncronize input after an error.
 * It is based on the assumption that the keywords start
 * at column 0
 */
void
yyerrsync()
{
	int c;
	while (curp = fgets(buffer, sizeof(buffer), file)) {
		if (!isspace(*curp))
			return;
	}
}

void
read_profile(char *name)
{
	UINT4 ipaddr;
	UINT4 mask;
	int len;
	struct stat st;
	
	line = 1;
	filename = name;

	if ((file = fopen(filename, "r")) == NULL) {
		error("can't open `%s': %s", filename, strerror(errno));
		exit(1);
	}

	curp = NULL;
	clear_channels();
	yyparse();
	fclose(file);
}

void
debug_profile()
{
	yydebug = 1;
}

void
register_address(char *name, IPADDR addr, UINT4 netmask)
{
	list_t streamlist = NULL;
	stream_t stream;
	network_t network;
	network_t all;
	
	network.addr = addr;
	network.netmask = netmask;
	all.addr = 0;
	all.netmask = 0;
	
	stream.dir = DIR_SRC;
	stream.direct.src = NULL;
	stream.direct.dst = NULL;
	stream.except.src = NULL;
	stream.except.dst = NULL;
	list_alloc(&stream.direct.src, &network, sizeof(network));
	list_alloc(&stream.direct.dst, &all, sizeof(all));
	list_alloc(&streamlist, &stream, sizeof(stream));

	stream.dir = DIR_DST;
	stream.direct.src = NULL;
	stream.direct.dst = NULL;
	list_alloc(&stream.direct.dst, &network, sizeof(network));
	list_alloc(&stream.direct.src, &all, sizeof(all));
	list_alloc(&streamlist, &stream, sizeof(stream));
	register_stream_list(name, streamlist);
}
