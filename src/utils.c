/* This file is part of IPACCT
   Copyright (C) 1999,2000,2001,2002,2003,2004,2005,2008 Sergey Poznyakoff

   Ipacct is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   Ipacct is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Ipacct.  If not, see <http://www.gnu.org/licenses/>. */

#if defined(HAVE_CONFIG_H)
# include <config.h>
#endif

#include <stdlib.h>
#include <string.h>
#include <pcap.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <stdarg.h>
#include <syslog.h>
#include <argp.h>
#include "ipacct.h"
 
int use_stderr = 1;

char *
copy_argv(int argc, char **argv, int num)
{
	int i, len = 0;
	char *p, *q;
	char *cmdbuf;
	
	for (i = num; i < argc; i++) 
		len += strlen(argv[i]) + 1;
	
	cmdbuf = malloc(len + 1);
	if (!cmdbuf) 
		lowcore();

	for (p = cmdbuf, argv += num; *argv; argv++) {
		q = *argv;
		while (*p++ = *q++);
		p[-1] = ' ';
	}
	p[-1] = 0;
	return cmdbuf;
}
		

char *
read_expr(char *name)
{
	FILE *fp;
	struct stat st;
	char *cmdbuf;
	
	if (stat(name, &st)) 
		die(1, "can't stat file %s: %s", name, strerror(errno));
		
	if ((fp = fopen(name, "r")) == NULL) {
		die(1, "can't open file %s: %s", name, strerror(errno));
	}

	cmdbuf = malloc(st.st_size+1);
	if (!cmdbuf)
		lowcore();

	if (fread(cmdbuf, 1, st.st_size, fp) != st.st_size)
		die(1, "read error on `%s': %s",
		    name,
		    strerror(errno));
	fclose(fp);
	cmdbuf[st.st_size] = 0;
	return cmdbuf;
}

pid_t
get_pid_from_file(char *name)
{
	FILE *fp;
	char buf[80];
	pid_t pid;
	
	fp = fopen(name, "r");
	if (!fp) {
		if (errno != ENOENT) {
			error("can't open file `%s': %s", name,
				strerror(errno)); 
		}
		return -1;
	}
	fgets(buf, sizeof(buf), fp);
	pid = strtol(buf, NULL, 10);
	return (pid > 0) ? pid : -1;	
}

void
init_syslog()
{
	use_stderr = 0;
	openlog(program_invocation_short_name, LOG_PID, LOGFACILITY);
}

void
error(char *fmt, ...)
{
	va_list ap;
		
       	va_start(ap, fmt);
	if (use_stderr) {
		fprintf(stderr, "%s: ", program_invocation_short_name);
		vfprintf(stderr, fmt, ap);
		fprintf(stderr, "\n");
	} else {
		vsyslog(LOG_ERR, fmt, ap);
	}
	va_end(ap);
}

void
warning(char *fmt, ...)
{
	va_list ap;
		
       	va_start(ap, fmt);
	if (use_stderr) {
		fprintf(stderr, "%s: ", program_invocation_short_name);
		vfprintf(stderr, fmt, ap);
		fprintf(stderr, "\n");
	} else {
		vsyslog(LOG_WARNING, fmt, ap);
	}
	va_end(ap);
}


void
die(int code, char *fmt, ...)
{
	va_list ap;

	va_start(ap, fmt);
	fprintf(stderr, "%s: ", program_invocation_short_name);
	vfprintf(stderr, fmt, ap);
	fprintf(stderr, "\n");
	va_end(ap);
	exit(code);
}




