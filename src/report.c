/* This file is part of IPACCT
   Copyright (C) 1999,2000,2001,2002,2003,2004,2005,2008 Sergey Poznyakoff

   Ipacct is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   Ipacct is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Ipacct.  If not, see <http://www.gnu.org/licenses/>. */

#if defined(HAVE_CONFIG_H)
# include <config.h>
#endif

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <errno.h>
#include <syslog.h>
#include <time.h>
#include "ipacct.h"
#include "report.h"

#define CHNL_FILE   0
#define CHNL_UDP    1
#define CHNL_SYSLOG 2

typedef struct {
	int type;
	int absolute;
	int ready;
	union {
		struct {
			IPADDR ipaddr;
			int port;
			int socket;
			int size;
			Stat_reply *data;
		} udp;
		struct {
			int facility;
			int priority;
		} syslog;
		struct {
			char *name;
			char timestr[64];
			FILE *fp;
		} file;
	} v;
} Channel;

#define MAX_CHANNEL 16

int num_channels;
static Channel channel[MAX_CHANNEL];

void
clear_channels()
{
	int i;

	for (i = 0; i < num_channels; i++)
		if (channel[i].type == CHNL_FILE)
			free(channel[i].v.file.name);
	num_channels = 0;
}

int
add_file_channel(char *name, int abs)
{
	Channel *cp;
	
	if (num_channels == MAX_CHANNEL)
		return -1;

	cp = &channel[ num_channels++ ];
	cp->type = CHNL_FILE;
	cp->ready = 0;
	cp->absolute = abs;
	cp->v.file.name = strdup(name);
	return 0;
}

int
add_syslog_channel(int abs, int facility, int priority)
{
	Channel *cp;
	
	if (num_channels == MAX_CHANNEL)
		return -1;

	cp = &channel[ num_channels++ ];
	cp->type = CHNL_SYSLOG;
	cp->ready = 0;
	cp->absolute = abs;
	cp->v.syslog.facility = facility;
	cp->v.syslog.priority = priority;
	return 0;
}

int
add_udp_channel(IPADDR ipaddr, int port, int abs)
{
	Channel *cp;
	
	if (num_channels == MAX_CHANNEL)
		return -1;

	cp = &channel[ num_channels++ ];
	cp->type = CHNL_UDP;
	cp->ready = 0;
	cp->absolute = abs;
	cp->v.udp.ipaddr = ipaddr;
	cp->v.udp.port = port;
	return 0;
}

void
open_channel(Channel *cp, time_t time)
{
	FILE *fp;
	int fd;
	struct sockaddr salocal;
	struct sockaddr_in *sin;
	int length;
	
	cp->ready = 0;
	switch (cp->type) {
	case CHNL_FILE:
		fp = fopen(cp->v.file.name, "a");
		if (!fp) {
			error("cannot open file `%s' for append: %s",
			      strerror(errno));
			break;
		}
		cp->v.file.fp = fp;
		strncpy(cp->v.file.timestr,
			asctime(localtime(&time)), sizeof(cp->v.file.timestr));
		/* kill trailing newline */
		if ((length = strlen(cp->v.file.timestr)) > 0)
			cp->v.file.timestr[length-1] = 0;
		cp->ready = 1;
		break;
		
	case CHNL_UDP:
		cp->v.udp.data = malloc(length = reply_size(MAXADDR));
		if (!cp->v.udp.data) {
			error("not enough memory to allocate reply structure");
			return;
		}
		cp->v.udp.data->n_addr = 0;
		cp->v.udp.data->timestamp = htonl(time);
		fd = socket(AF_INET, SOCK_DGRAM, 0);
		if (fd == -1) {
			error("socket: %s", strerror(errno));
			free(cp->v.udp.data);
			return;
		}
		length = sizeof(salocal);
		sin = (struct sockaddr_in *) &salocal;
		memset(sin, 0, sizeof(sin));
		sin->sin_family = AF_INET;
		sin->sin_addr.s_addr = INADDR_ANY;
		sin->sin_port = 0;
		if (bind(fd, (struct sockaddr*) sin, length) < 0) {
			error("bind: %s", strerror(errno));
			close(fd);
			free(cp->v.udp.data);
			return;
		}
		cp->v.udp.socket = fd;
		cp->ready = 1;
		break;
		
	case CHNL_SYSLOG:
		closelog();
		openlog(NULL, LOG_NDELAY, cp->v.syslog.facility);
		cp->ready = 1;
		break;
	}
}

void
flush_udp(Channel *cp)
{
	struct sockaddr salocal;
	struct sockaddr_in *sin = (struct sockaddr_in*) &salocal;
	int length;

	if (cp->v.udp.data->n_addr == 0)
		return;
	
	length = reply_size(cp->v.udp.data->n_addr);
	cp->v.udp.data->n_addr = htonl(cp->v.udp.data->n_addr);

	memset(sin, 0, sizeof(sin));

	sin->sin_family = AF_INET;
	sin->sin_addr.s_addr = cp->v.udp.ipaddr;
	sin->sin_port = cp->v.udp.port;
	
	if (sendto(cp->v.udp.socket,
	       cp->v.udp.data, length,
	       0,
	       (struct sockaddr*)sin,
	       sizeof(struct sockaddr_in)) < 0)
		error("sendto: %s", strerror(errno));

	cp->v.udp.data->n_addr = 0;
}

void
close_channel(Channel *cp)
{
	switch (cp->type) {
	case CHNL_FILE:
		fclose(cp->v.file.fp);
		break;
	case CHNL_UDP:
		flush_udp(cp);
		close(cp->v.udp.socket);
		free(cp->v.udp.data);
		break;
	case CHNL_SYSLOG:
		closelog();
		init_syslog();
		break;
	}
	cp->ready = 0;
}

void
report_channel(Channel *cp, stat_t *stat)
{
	Stat_data *sp;
	int len;
	struct counter *ctr;
	
	if (cp->absolute) {
		ctr = &stat->bytes;
	} else {
		ctr = &stat->diff;
	}

	switch (cp->type) {
	case CHNL_FILE:
		fprintf(cp->v.file.fp,
			"%-26.26s%-17.17s %12lu %12lu\n",
			cp->v.file.timestr,
			stat->name,
			ctr->out,
			ctr->in);
		break;

	case CHNL_SYSLOG:
		syslog(cp->v.syslog.priority,
		       "%-17.17s %12lu %12lu\n",
		       stat->name,
		       ctr->out,
		       ctr->in);
		break;

	case CHNL_UDP:
		if (cp->v.udp.data->n_addr == MAXADDR)
			flush_udp(cp);
		sp = &cp->v.udp.data->stat[cp->v.udp.data->n_addr++];
		strncpy(sp->name, stat->name, sizeof(sp->name));
		sp->in = htonl(ctr->in);
		sp->out = htonl(ctr->out);
		break;
	}
}

void
open_report(time_t time)
{
	int i;

	for (i = 0; i < num_channels; i++) 
		open_channel(channel+i, time);
}
	
void
close_report()
{
	int i;

	for (i = 0; i < num_channels; i++)
		if (channel[i].ready)
			close_channel(channel+i);
}

void
report(stat_t *stat)
{
	int i;

	for (i = 0; i < num_channels; i++)
		if (channel[i].ready)
			report_channel(channel+i, stat);
}

