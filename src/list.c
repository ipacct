/* This file is part of IPACCT
   Copyright (C) 1999,2000,2001,2002,2003,2004,2005,2008 Sergey Poznyakoff

   Ipacct is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   Ipacct is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Ipacct.  If not, see <http://www.gnu.org/licenses/>. */

/* Generic singly-linked lists */
/* $Id: list.c,v 1.5 2008/07/07 14:35:27 gray Exp $ */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif
#include <stdlib.h>
#include <ipacct.h>
#include <syslog.h>

struct list_data {
	struct list_data *next;
	char data[1];
};

struct _list {
	 struct list_data *head, *tail;
};

#define ELSIZE(s) (sizeof(struct list_data) - 1 + (s))

static list_t * _list_alloc_internal(size_t size);

struct list_data *
_list_alloc_data(size_t size)
{
	struct list_data *p;

	p = malloc(ELSIZE(size));
	if (!p) {
		syslog(LOG_CRIT, "not enough memory: exiting");
		abort();
	}
	p->next = NULL;
	return p;
}

void
list_create(list_t *listp)
{
	*listp = malloc(sizeof(**listp));
	if (!*listp) {
		syslog(LOG_CRIT, "not enough memory: exiting");
		abort();
	}
	(*listp)->head = (*listp)->tail = NULL;
}

void *
list_alloc(list_t *listp, void *data, size_t size)
{
	struct list_data *dp;
	list_t list;

	if (!*listp)
		list_create(listp);
	list = *listp;
	dp = _list_alloc_data(size);
	if (!list->tail)
		list->head = dp;
	else
		list->tail->next = dp;
	list->tail = dp;
	memcpy(dp->data, data, size);
	return dp->data;
}


void
list_free(list_t *listp)
{
	struct list_data *this, *next;

	if (!*listp)
		return;
	this = (*listp)->head;
	while (this) {
		next = this->next;
		free(this);
		this = next;
	}
	free(*listp);
	*listp = NULL;
}

void
list_iterate(list_t list, int (*fun)(void *, void *), void *data)
{
	struct list_data *this, *next;

	if (!list)
		return;
	for (this = list->head; this; this = this->next)
		if ((*fun)(this->data, data))
			break;
}
	
