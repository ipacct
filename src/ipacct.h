/* This file is part of IPACCT
   Copyright (C) 1999,2000,2001,2002,2003,2004,2005,2008 Sergey Poznyakoff

   Ipacct is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   Ipacct is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Ipacct.  If not, see <http://www.gnu.org/licenses/>. */

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/in_systm.h>
#include <netinet/ip.h>
#include <netinet/udp.h>
#include <net/if.h>
#include <netinet/if_ether.h>
#include <pcap.h>
#include <list.h>

#define MAX_PATH_LENGTH 255
#define MAX_FILENAME_LENGTH 255
#define MAX_NAME_LENGTH 32
#define TIME_TO_WAIT 5*60

#ifndef SYSCONFDIR
# define SYSCONFDIR "/etc"
#endif

#define STAT_DIR "/var/log/"
#define DUMP_FILE "ipacct.dump"
#define LOG_FILE "ipacct.log"
#define PROFILE "ipacct.conf"

#ifndef LOGFACILITY
# define LOGFACILITY LOG_LOCAL5
#endif

typedef unsigned long UINT4;
typedef UINT4 IPADDR;
#define ASCII_IP_LENGTH 64
#define ALLONES 0xfffffffful

typedef struct network_rec {
	IPADDR addr;
	UINT4 netmask;
} network_t;


#define DIR_SRC 0
#define DIR_DST 1

typedef struct {
	list_t src;
	list_t dst;
} flow_t;

typedef struct stream_rec {
	int dir;           /* stream direction */
	flow_t direct;
	flow_t except;
} stream_t;

struct counter {
	unsigned long in;
	unsigned long out;
};

typedef struct {
	char name[MAX_NAME_LENGTH];
	list_t streams;
	struct counter bytes;
	struct counter prev;
	struct counter diff;
} stat_t;



void run(pcap_t *, pcap_handler);
void cleanup();

char * read_expr(char *file);
char * copy_argv(int argc, char **argv, int num);
pcap_handler lookup_pcap_callback (int type);
pid_t get_pid_from_file(char*);
void cons_raw_packet(u_char *, const struct pcap_pkthdr *, const u_char *);
void cons_ether_packet(u_char *, const struct pcap_pkthdr *, const u_char *);
void cons_ppp_packet(u_char *, const struct pcap_pkthdr *, const u_char *);

#define lowcore() die(1, "%s:%d: low core", __FILE__, __LINE__)

extern pcap_t *pd;
extern fd_set   readmask, writemask, exceptmask;
extern unsigned long total_bytes;
extern char *progname;
extern char statdir[];
extern char dump_name[];
extern char *log_name;
extern time_t start_time;
extern time_t slice_interval;
extern int fixed_clocks;
extern int verbose;
extern int foreground;
extern char *user;

char * ip_hostname(IPADDR ipaddr);
IPADDR get_ipaddr(char *host);
int good_ipaddr(char *addr);
char * ipaddr2str(char *buffer, IPADDR ipaddr);
IPADDR ipstr2long(char *ip_str);
IPADDR getmyip();

int add_udp_channel(IPADDR ipaddr, int port, int abs);
int add_file_channel(char *name, int abs);

void register_stream_list(char *name, list_t stream);

void error(char *fmt, ...);
void warning(char *fmt, ...);
void die(int code, char *fmt, ...);
	
