/* This file is part of IPACCT
   Copyright (C) 1999,2000,2001,2002,2003,2004,2005,2008 Sergey Poznyakoff 

   Ipacct is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   Ipacct is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Ipacct.  If not, see <http://www.gnu.org/licenses/>. */

#if defined(HAVE_CONFIG_H)
# include <config.h>
#endif
#include <stdlib.h>
#include <errno.h>
#include <limits.h>
#include <ipacct.h>
#include <syslog.h>
#include <time.h>
#include <string.h>

list_t targets;

static void verbose_report(stat_t *stat, struct ip *ip);

void
register_stream_list(char *name, list_t streamlist)
{
	stat_t stat;

	memset(&stat, 0, sizeof(stat));
	strncpy(stat.name, name, sizeof(stat.name));
	stat.name[sizeof(stat.name)-1] = 0;
	stat.streams = streamlist;
	list_alloc(&targets, &stat, sizeof(stat));
}

static int
_stream_free(void *streamp, void *unused)
{
	stream_t *stream = (stream_t *)streamp;
	list_free(&stream->direct.src);
	list_free(&stream->direct.dst);
	list_free(&stream->except.src);
	list_free(&stream->except.dst);
}

void
stream_list_free(list_t streamlist)
{
	list_iterate(streamlist, _stream_free, NULL);
	list_free(&streamlist);
}

static int
_stat_free(void *statp, void *unused)
{
	stat_t *stat = (stat_t*)statp;

	stream_list_free(stat->streams);
	return 0;
}

void
account_init()
{
	list_iterate(targets, _stat_free, NULL);
	list_free(&targets);
}

struct ipdata {
	struct ip *ip;
	size_t length;
	stat_t *stat;
	int dir;
	IPADDR addr[2];
	int match;
};


int
_network_match(void *networkp, void *ipdatap)
{
	network_t *np = (network_t*)networkp;
	struct ipdata *ipd = (struct ipdata*)ipdatap;

	if (verbose > 1) {
		char buf1[16], buf2[16];
		printf("# comparing %s/%s",
		       ipaddr2str(buf1, np->addr),
		       ipaddr2str(buf2, np->netmask));
	}
	if (np->addr == (ipd->addr[ipd->dir] & np->netmask)) {
		if (verbose > 1)
			printf(" OK\n");
		ipd->match++;
		return 1;
	}
	if (verbose > 1)
		printf(" no\n");
	return 0;
}

int
lookup_address(flow_t *flow, struct ipdata *ipd)
{
	ipd->match = 0;
	if (verbose > 1)
		printf("# trying source flow:\n");
	ipd->dir = DIR_SRC;
	list_iterate(flow->src, _network_match, ipd);
	if (verbose > 1)
		printf("# trying dest flow:\n");
	ipd->dir = DIR_DST;
	list_iterate(flow->dst, _network_match, ipd);
	if (verbose > 1) 
		printf("# %d matches\n", ipd->match);
	return ipd->match == 2;
}

int
_account_check_stream(void *streamp, void *ipp)
{
	stream_t *stream = (stream_t *)streamp;
	struct ipdata *ipd = (struct ipdata *)ipp;
	int match = 0;

	ipd->dir = stream->dir;
	
	ipd->addr[DIR_SRC] = ipd->ip->ip_src.s_addr;
	ipd->addr[DIR_DST] = ipd->ip->ip_dst.s_addr;
	if (lookup_address(&stream->direct, ipd)
	    && !lookup_address(&stream->except, ipd)) {
		/*FIXME: bytes should be ctr[2] */
		if (stream->dir == DIR_SRC)
			ipd->stat->bytes.out += ipd->length;
		else
			ipd->stat->bytes.in += ipd->length;
		match++;
		if (foreground && verbose) 
			verbose_report(ipd->stat, ipd->ip);
	}
	return match;
}

int
_account_check_stat(void *statp, void *ipp)
{
	struct ipdata *ipd = (struct ipdata *)ipp;

	ipd->stat = statp;
	if (verbose > 1) 
		printf("# stat %s\n", ipd->stat->name);
	list_iterate(ipd->stat->streams, _account_check_stream, ipd);
	return 0;
}

void
account(struct ip *ip)
{
	struct ipdata ipd;

	ipd.ip = ip;
	ipd.length = ntohs(ip->ip_len);
	if (verbose > 1) {
		char buf1[16], buf2[16];
		printf("# from %s to %s\n",
		       ipaddr2str(buf1, ip->ip_src.s_addr),
		       ipaddr2str(buf2, ip->ip_dst.s_addr));
	}
	list_iterate(targets, _account_check_stat, &ipd);
}

void
verbose_report(stat_t *stat, struct ip *ip)
{
	char buf1[16], buf2[16];
	
	printf("%s: (%x) %s > (%x) %s %d: %u %u\n",
	       stat->name,
	       ip->ip_src.s_addr,
	       ipaddr2str(buf1, ip->ip_src.s_addr),
	       ip->ip_dst.s_addr,
	       ipaddr2str(buf2, ip->ip_dst.s_addr),
	       ntohs(ip->ip_len),
	       stat->bytes.in, stat->bytes.out);
}

int
_stat_diff(void *statp, void *unused)
{
	stat_t *sp = (stat_t *)statp;
	
	if (sp->bytes.in >= sp->prev.in)
		sp->diff.in = sp->bytes.in - sp->prev.in;
	else
		sp->diff.in = (ULONG_MAX - sp->prev.in) + sp->bytes.in;
	if (sp->bytes.out >= sp->prev.out)
		sp->diff.out= sp->bytes.out- sp->prev.out;
	else
		sp->diff.out = (ULONG_MAX - sp->prev.out) + sp->bytes.out;
	/* Generally speaking, the following is incorrect, but we do
	 * this as a favor for users:
	 * 	When one of the differences is 0 we assume the link is
	 *	down and therefore we do not count other diff even if it 
	 *	is non-zero.
	 */
	if (sp->diff.in == 0 || sp->diff.out == 0) {
		sp->diff.in = sp->diff.out = 0;
		sp->bytes = sp->prev;
	} else
		sp->prev = sp->bytes;

	return 0;
}

int
_stat_report(void *statp, void *fp)
{
	stat_t *sp = (stat_t *)statp;

	if (fp) {
		fprintf(fp, "%s\t%lu\t%lu\n",
			sp->name,
			sp->bytes.in,
			sp->bytes.out);
	}
	report(sp);
	return 0;
}


void
dump_table(time_t time)
{
	FILE *dump_fp;
	char timestr[64];
	int len;

	/* Compute diffs. This must be done before eventual forking */
	list_iterate(targets, _stat_diff, NULL);

	if (open_process())
		return;
	
	strncpy(timestr, asctime(localtime(&time)), sizeof(timestr));
	/* kill trailing newline */
	if ((len = strlen(timestr)) > 0)
		timestr[len-1] = 0;

	dump_fp = fopen(dump_name, "w");
	if (!dump_fp) 
		error("can't open `%s': %s", dump_name, strerror(errno));
	else {
		fprintf(dump_fp,
			"Started at %s", asctime(localtime(&start_time)));
		fprintf(dump_fp,
			"Slice for  %s\n", timestr);
	}

	open_report(time);
	list_iterate(targets, _stat_report, dump_fp);

	if (dump_fp) 
		fclose(dump_fp);
	close_report();
	close_process();
}
	
	
static char buf[128];
int doprompt;

char *
moreinput(char *buf, size_t bufsize)
{
	if (doprompt)
		printf("%% ");
	return fgets(buf, bufsize, stdin);
}

int
test_shell()
{
	char *tok;
	int c;
	struct ip ip;
	
#define opttkn() (tok = strtok(NULL, " \t"))
#define nextkn() if (opttkn() == NULL) {\
 printf("arg count\n");\
 continue; }  
#define chktkn() if (!tok) { \
 printf("arg count\n");\
 continue; }

	printf("** TEST MODE **\n");
	doprompt = isatty(fileno(stdin));
	while (tok = moreinput(buf, sizeof(buf))) {
		while (*tok && isspace(*tok))
			tok++;
		c = strlen(tok);
		if (c > 1 && tok[c-1] == '\n')
			tok[c-1] = 0;
		c = *tok++;
		tok = strtok(tok, " \t");
		switch (c) {
		case 0:
		case '#':
			continue;
		case '?':
			printf("p SRC DEST [LEN]\n");
			printf("v {NUM|?}\n");
			printf("q\n");
			break;
		case 'p':
			chktkn();
			ip.ip_src.s_addr = ipstr2long(tok);
			nextkn();
			ip.ip_dst.s_addr = ipstr2long(tok);
			if (opttkn())
				ip.ip_len = atoi(tok);
			else
				ip.ip_len = sizeof(ip);
			ip.ip_len = htons(ip.ip_len);
			account(&ip);
			break;
		case 'v':
			if (tok[0] == '?')
				printf("verbose is %d\n", verbose);
			else
				verbose = atoi(tok);
			break;
		case 'q':
			return 0;
		default:
			printf("no command\n");
		}
	}
	return 0;
}			
			
