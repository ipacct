/* This file is part of IPACCT
   Copyright (C) 1999,2000,2001,2002,2003,2004,2005,2008 Sergey Poznyakoff 

   Ipacct is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   Ipacct is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Ipacct.  If not, see <http://www.gnu.org/licenses/>. */

#if defined(HAVE_CONFIG_H)
# include <config.h>
#endif

#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <stdlib.h>
#include <unistd.h>
#include <pwd.h>
#include <syslog.h>
#include <time.h>
#include <errno.h>
#include <signal.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <getopt.h>
#include <pcap.h>
#include <argp.h>
#include <ipacct.h>

#ifndef RETSIGTYPE
# define RETSIGTYPE void
#endif

RETSIGTYPE sig_hup();
RETSIGTYPE sig_usr1();
RETSIGTYPE sig_usr2();
RETSIGTYPE sig_child();
static pcap_t  * init(char *device, char *cmd);

int dump_bytecode;        /* dump the packet-matched code and exit */
int promisc_off;          /* do not put the interface into promiscuous mode */
int optimize_expr;        /* optimize compiled expression */
int foreground;           /* remain in the foreground */
int fixed_clocks;         /* use fixed clocks */
int single_process = 0;   /* don't fork separate processes for accounting */
int verbose = 0;
char *profile = SYSCONFDIR "/" PROFILE;
char dump_name[MAX_PATH_LENGTH];
char *log_name;

char *device;
int snaplen = 96;
unsigned int localnet = 0, netmask = 0;
time_t start_time;
time_t slice_interval = TIME_TO_WAIT;
static pcap_t *pcap;
static char pidfile[MAX_PATH_LENGTH];

static char *infile = NULL;
static char *expr = NULL;
static time_t ttw = 0;
static int syntax_check = 0;    /* check the config file and exit */
static int test_mode = 0;
char *user = NULL;

const char *argp_program_version = "ipacct (" PACKAGE ") " VERSION;
static char doc[] = "ip traffic accounting daemon";

static struct argp_option options[] = {
	{ "dump-bytecode", 'b', 0, 0,
	  "Dump compiled packet comparison bytecode to stdout and exit", 0},
	{ "config", 'c', "FILE", 0,
	  "Specify alternative configuration file", 0 },
	{ "foreground", 'f', NULL, 0,
	  "Run in foreground", 0 },
	{ "file", 'F', "FILE", OPTION_HIDDEN,
	  "", 0 },
	{ "interface", 'i', "DEVICE", 0,
	  "Specify interface to listen on", 0 },
	{ "log-file", 'l', "FILE", 0,
	  "Set log file name", 0 },
	{ "optimize", 'O', NULL, 0,
	  "Optimize bytecode", 0 },
	{ "no-promiscuous-mode", 'p', NULL, 0,
	  "Do not put the inteface into promiscuous mode", 0 },
	{ "single-process", 's', NULL, 0,
	  "Run in single-process mode", 0 },
	{ "syntax-check", 'S', NULL, 0,
	  "Check config file for errors and exit", 0 },
	{ "timeslice", 't', "NUMBER", 0,
	  "Set timeslice value in seconds", 0 },
	{ "test-shell", 'T', NULL, 0,
	  "Start test shell", 0 },
	{ "verbose", 'v', NULL, 0,
	  "Increase verbosity level", 0 },
	{ "fixed-clocks", 'x', NULL, 0,
	  "Dump statistics at precise time intervals", 0 },
	{ "user", 'u', "USER-NAME", 0,
	  "Run with this user privileges", 0 },
	{NULL, 0, NULL, 0, NULL, 0}
};


/*ARGSUSED*/
static error_t
parse_opt(int key, char *arg, struct argp_state *state)
{
	switch (key) {
	case 'b':
		dump_bytecode++;
		break;
	case 'c':
		profile = arg;
		break;
	case 'f':
		foreground++;
		break;
	case 'F':
		infile = arg;
		break;
	case 'i':
		device = arg;
		break;
	case 'l':
		log_name = arg;
		break;
	case 'O':
		optimize_expr++;
		break;
	case 'p':
		promisc_off++;
		break;
	case 's':
		single_process++;
		break;
	case 'S':
		syntax_check++;
		break;
	case 't':
		ttw = atoi(arg);
		if (ttw <= 0) 
			die(1, "invalid timeslice value (%d)", ttw);
		break;
	case 'T':
		test_mode++;
		foreground++;
		break;
	case 'v':
		verbose++;
		if (verbose > 2)
			debug_profile();
		break;
	case 'x':
		fixed_clocks++;
		break;
	case 'u':
		user = arg;
		break;
	default:
		return ARGP_ERR_UNKNOWN;
	}
	return 0;
}

static struct argp argp = {
	options,
	parse_opt,
	NULL,
	doc,
	NULL,
	NULL, NULL
};

/* Change to the given uid/gid. Clear the supplementary group list.
   On success returns 0.
   On failure returns 1 (or exits, depending on topt settings. See
   anubis_error) */
static int
change_privs (uid_t uid, gid_t gid)
{
	int rc = 0;
	gid_t emptygidset[1];

	/* Reset group permissions */
	emptygidset[0] = gid ? gid : getegid();
	if (geteuid() == 0 && setgroups(1, emptygidset)) {
		error ("setgroups(1, %lu) failed: %s",
			(u_long) emptygidset[0],
		       strerror (errno));
		rc = 1;
	}

	/* Switch to the user's gid. On some OSes the effective gid must
	   be reset first */

#if defined(HAVE_SETEGID)
	if ((rc = setegid(gid)) < 0)
		error ("setegid(%lu) failed: %s",
		       (u_long) gid, strerror (errno));
#elif defined(HAVE_SETREGID)
	if ((rc = setregid(gid, gid)) < 0)
		error ("setregid(%lu,%lu) failed: %s",
			(u_long) gid, (u_long) gid, strerror (errno));
#elif defined(HAVE_SETRESGID)
	if ((rc = setresgid(gid, gid, gid)) < 0)
		error ("setresgid(%lu,%lu,%lu) failed: %s",
		       (u_long) gid,
		       (u_long) gid,
		       (u_long) gid,
		       strerror (errno));
#endif

	if (rc == 0 && gid != 0) {
		if ((rc = setgid(gid)) < 0 && getegid() != gid) 
			error ("setgid(%lu) failed: %s",
			       (u_long) gid,
			       strerror (errno));
		if (rc == 0 && getegid() != gid) {
			error("cannot set effective gid to %lu: %s",
			      (u_long) gid,
			      strerror (errno));
			rc = 1;
		}
	}

	/* now reset uid */
	if (rc == 0 && uid != 0) {
		uid_t euid;

		if (setuid(uid)
		    || geteuid() != uid
		    || (getuid() != uid
			&& (geteuid() == 0 || getuid() == 0))) {
			
#if defined(HAVE_SETREUID)
			if (geteuid() != uid) {
				if (setreuid(uid, -1) < 0) {
					error ("setreuid(%lu,-1) failed: %s",
					       (u_long) uid,
					       strerror (errno));
					rc = 1;
				}
				if (setuid(uid) < 0) {
					error ("second setuid(%lu) failed: %s",
					       (u_long) uid,
					       strerror (errno));
					rc = 1;
				}
			} else
#endif
				{
					error ("setuid(%lu) failed: %s",
					       (u_long) uid,
					       strerror (errno));
					rc = 1;
				}
		}
	

		euid = geteuid();
		if (uid != 0 && setuid(0) == 0) {
			error ("seteuid(0) succeeded when it should not");
			rc = 1;
		} else if (uid != euid && setuid(euid) == 0) {
			error ("cannot drop non-root setuid privileges");
			rc = 1;
		}
	}
	return rc;
}

void
change_user ()
{
	struct passwd *pwd;
	
	if (user == NULL)
		return;
	
	pwd = getpwnam (user);
	if (pwd) {
		if (change_privs (pwd->pw_uid, pwd->pw_gid))
			exit (1);
	
		chdir (pwd->pw_dir);
		syslog (LOG_INFO, "UID:%d (%s), GID:%d, EUID:%d, EGID:%d",
			(int) getuid (), pwd->pw_name, (int) getgid (),
			(int) geteuid (), (int) getegid ());
	}
}

int
main(int argc, char **argv)
{
	int c, type;
	pcap_handler callback;
	pid_t pid;
	int index;
	
	if (argp_parse(&argp, argc, argv, 0, &index, NULL))
		exit(1);

	read_profile(profile);
	if (syntax_check)
		return 0;
	if (test_mode)
		return test_shell();
	
	alloc_snap_buffer();

	if (infile) {
		if (index < argc) 
			die(1, "excess arguments after %s", argv[index]);
		expr = read_expr(infile);
	} else if (index < argc) 
		expr = copy_argv(argc, argv, index);

	
	pcap = init(device, expr);
	if (!(callback = lookup_pcap_callback(type = pcap_datalink(pcap))))
		die(1, "unknown data link type 0x%x", type);

	start_time = time(NULL);
	if (ttw)
		slice_interval = ttw;
	
	if (!foreground) {
		int i;
		pid_t pid = fork();
		if (pid < 0) 
			die(1, "cannot fork: %s", strerror(errno));
		if (pid > 0) {
			/* Parent branch */
			FILE *fp;
			/* write pid file */
			if ((fp = fopen(pidfile, "w")) != NULL) {
				fprintf(fp, "%d\n", pid);
				fclose(fp);
			} else {
				error("can't write pid file %s: %s",
				      pidfile, strerror(errno));
			}
			exit(0);
		}
		/* Child branch */
		chdir("/");
		for (i = 0; i < 3; i++)
			close(i);
		setsid();
		init_syslog();
	}

	change_user();
	run(pcap, callback);

	cleanup();
	return 0;
}

pcap_t  *
init(char *device, char *cmdbuf)
{
	char buf[PCAP_ERRBUF_SIZE];
	struct bpf_program bpfcode;
	pcap_t *pd;
	pid_t pid;
	
	if (!device)
		if (!(device = pcap_lookupdev(buf)))
			die(1, buf);

	sprintf(pidfile, "/var/run/ipacct-%s.pid", device);
	if (!foreground && (pid = get_pid_from_file(pidfile)) > 0) 
		die(1, "the ipacct daemon seems to be running under pid %d.\n"
		    "If it is not, remove `%s' file and start ipacct again",
		    pid, pidfile);
	
	
	if (pd = pcap_open_live(device, snaplen, !promisc_off, 1000, buf)) {
		if ((pcap_lookupnet(device,
				    (u_int *) &localnet, (u_int *) & netmask,
				    buf)) < 0)
			die(1, buf);
	} else
		die(1, buf);
	
	setuid(getuid());

	memset(&bpfcode, 0, sizeof(bpfcode));

	if (pcap_compile(pd, &bpfcode, cmdbuf, optimize_expr, netmask) < 0)
		die(1, pcap_geterr(pd));

	if (dump_bytecode) {
		bpf_dump(&bpfcode, dump_bytecode);
		exit(0);
	}
	if (pcap_setfilter(pd, &bpfcode) < 0)
		die(1, pcap_geterr(pd));

	signal(SIGPIPE, SIG_IGN);
	signal(SIGHUP, sig_hup);
        signal(SIGINT, (RETSIGTYPE (*)())cleanup);
	signal(SIGTERM, (RETSIGTYPE (*)())cleanup);
	signal(SIGUSR1, sig_usr1);
	signal(SIGUSR2, sig_usr2);
	signal(SIGCHLD, sig_child);

/*	cons_init();*/
	return pd;
}


int
open_process()
{
	pid_t pid;

	if (single_process)
		return 0;

	pid = fork();
	if (pid > 0) 
		return 1;
	else if (pid < 0) {
		error("fork: %s", strerror(errno));
		return -1;
	}
	
	signal(SIGHUP, SIG_IGN);
	signal(SIGUSR1, SIG_IGN);
	signal(SIGUSR2, SIG_IGN);
	signal(SIGCHLD, SIG_IGN);
	return 0;
}

int
close_process()
{
	if (!single_process)
		exit(0);
}

void
re_read_profile(char *name)
{
	account_init();
	read_profile(name);
}

void
cleanup()
{
	pcap_close(pcap);
	if (!foreground)
		unlink(pidfile);
	exit(0);
}

RETSIGTYPE
sig_hup()
{
	syslog(LOG_INFO, "reconfiguring");
	re_read_profile(profile);
	signal(SIGHUP, sig_hup);
}

RETSIGTYPE
sig_usr1(sig)
	int sig;
{
	dump_table(time(NULL));
	signal(SIGUSR1, sig_usr1);
}

RETSIGTYPE
sig_usr2()
{
}

RETSIGTYPE
sig_child(int sig)
{
	int status;
	while (waitpid((pid_t)-1, &status, WNOHANG) > 0)
		;
	signal(sig, sig_child);
}

