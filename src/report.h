/* This file is part of IPACCT
   Copyright (C) 1999,2000,2001,2002,2003,2004,2005,2008 Sergey Poznyakoff

   Ipacct is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   Ipacct is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Ipacct.  If not, see <http://www.gnu.org/licenses/>. */

#ifndef __report_h
#define __report_h

#define MAXADDR 64

typedef struct {
	char name[MAX_NAME_LENGTH];
	unsigned long in;
	unsigned long out;
} Stat_data;

typedef struct {
	int n_addr;
	unsigned long addr[1]; /* actually n_addr items */
} Stat_request;

typedef struct {
	int n_addr;
	time_t timestamp;
	Stat_data stat[1];
} Stat_reply;

#define reply_size(n) (sizeof(Stat_reply) + (n-1)*sizeof(Stat_data))
#define request_size(n) (sizeof(Stat_request + (n-1)*sizeof(unsigned long))

#endif
