/* This file is part of IPACCT
   Copyright (C) 1999,2000,2001,2002,2003,2004,2005,2008 Sergey Poznyakoff

   Ipacct is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   Ipacct is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Ipacct.  If not, see <http://www.gnu.org/licenses/>. */

#if defined(HAVE_CONFIG_H)
# include <config.h>
#endif
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include "ipacct.h"

void
cons_ether_init()
{
}

void
cons_ether_packet(u_char *user, const struct pcap_pkthdr *h, const u_char *p)
{
	struct ether_header ep;
	u_short ether_type;
	int caplen = h->caplen;
	int length = h->len;

	if (p && caplen > 0) {
		if (caplen < sizeof(ep)) {
			warning("caplen < sizeof(struct ether_header): caplen = %d, length = %d", caplen, length);		
			return;
		}
		bcopy ((char *) p, (char *) &ep, sizeof (ep));
		
		if (ether_type = ntohs(ep.ether_type)) {
			length -= sizeof(struct ether_header);
			total_bytes += length;
			switch (ether_type) {
			case ETHERTYPE_IP:
				ip_handler(p + sizeof(ep), length);
				break;
			default:
				break;
			}
		}
	}
}



