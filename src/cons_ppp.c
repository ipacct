/* This file is part of IPACCT
   Copyright (C) 1999,2000,2001,2002,2003,2004,2005,2008 Sergey Poznyakoff

   Ipacct is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   Ipacct is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Ipacct.  If not, see <http://www.gnu.org/licenses/>. */

#if defined(HAVE_CONFIG_H)
# include <config.h>
#endif
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include "ipacct.h"

#define PPP_HDRLEN 4

void
cons_ppp_packet(u_char *user, const struct pcap_pkthdr *h, const u_char *p)
{
	u_int length = h->len;
	u_int caplen = h->caplen;
	const struct ip *ip;

	if (caplen < PPP_HDRLEN) {
		return;
	}

	length -= PPP_HDRLEN;
	ip = (struct ip *)(p + PPP_HDRLEN);
	ip_handler(ip, length);
}



