/* This file is part of IPACCT
   Copyright (C) 1999,2000,2001,2002,2003,2004,2005,2008 Sergey Poznyakoff

   Ipacct is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   Ipacct is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Ipacct.  If not, see <http://www.gnu.org/licenses/>. */

#if defined(HAVE_CONFIG_H)
# include <config.h>
#endif
#include <stdlib.h>
#include "ipacct.h"

#define min(a,b) (a<b) ? (a):(b)

static u_char *buf;
extern int      snaplen;

void
alloc_snap_buffer()
{
	buf = malloc(snaplen);
	if (!buf)
		die(1, "can't alloc %d bytes for snap buffer", snaplen);
}

void 
ip_handler(struct ip *ip, unsigned int length)
{
	if (length >= sizeof(struct ip)) {
		if ((long) ip & (sizeof(long) - 1)) {
			bcopy((char *) ip, (char *) buf, min(length, snaplen));
			ip = (struct ip *) buf;
		} 
		
		account(ip);
	}
}


