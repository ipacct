# $Id: traflog.awk,v 1.1 2005/08/04 07:15:38 gray Exp $
# Copyright 2000, 2005 Sergey Poznyakoff
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA 02110-1301, USA.

#Field assignment
# 1   2  3    4       5        6          7      8   
#Sun Jun 20 00:04:06 1999 208.244.48.32	  30	1736

BEGIN {
	divisor = 1048576
	maxtraf = 4294960000
}

NF==8 {
	# Sanity check
	if (0+$7 < 0 || 0+$8 < 0 || 0+$7 > maxtraf || 0+$8 > maxtraf) {
		print NR ": Skipped (" $7 " " $8 ")" >"/dev/stderr"
		next
	} else {
		inbytes[$6] += 0+$7
		outbytes[$6] += 0+$8
	}
}

END {
	for (ip in inbytes) {
		print ip " " inbytes[ip] / divisor " " outbytes[ip] / divisor
	}
}


